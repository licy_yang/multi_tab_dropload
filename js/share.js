/**
 * 微信分享
 * @param site_url 网站url
 * @param share_info 分享信息
 * @param callback 回调函数，分享成功后调用
 */
/*调用例子
 * var g_share_info = {share_title:"给孩子不一样的暑假，婴童专场VIP邀请函",
                			share_desc:"给孩子不一样的暑假，婴童专场VIP邀请函",
                			share_url:"http://report.mplife.com/doing/nike",
                			share_img_url:"http://report.mplife.com/doing/nike/images20150629/letter_01.jpg"
                    		};
    		configWeixinShare("{{$SITE_URL}}",g_share_info);
 */
var g_share_info = {};
function configWeixinShare(site_url,share_info,callback){
	g_share_info = share_info;
	if( typeof(MultiLogin) != "function" ){
		configAppShare();
	}
	var request_url = document.URL;
	if( "request_url" in share_info ){
		request_url = share_info.request_url;
	}
	$.ajax({
		 url:site_url+"/api/good/get-weixin-key",
		 dataType:"jsonp",
		 data:{'request_url':request_url},
		 jsonp:"jsoncallback",
		 success:function(data){
			if( data.res != 100 ){
				return;
			}
			wx.config({
				debug: false,
				appId: data.extra.Result.AppId,
				timestamp: data.extra.Result.TimeStamp, 
				nonceStr: data.extra.Result.NonceStr, 
				signature: data.extra.Result.Signature,
				jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo']
			});
				 
			wx.ready(function() {
				var title = share_info.share_title;
				var desc = share_info.share_desc;
				var buyUrl = share_info.share_url;
				var imgUrl = share_info.share_img_url;
				//注册分享给朋友
				wx.onMenuShareAppMessage({
					title: title, 
					desc: desc, //描述
					link: buyUrl, //分享地址
					imgUrl: imgUrl, //图片地址
					trigger: function (res) {
						// 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
						//alert('用户点击发送给朋友');
					},
					success: function (res) {
						alert('感谢您的分享');
						if( $.isFunction(callback) ){
							callback();
						}
					},
					cancel: function (res) {
						alert('真可惜!还请下次介绍给您的朋友哦!');
					},
					fail: function (res) {
						alert("分享失败了!"+JSON.stringify(res));
					}
				});
			
				//注册朋友圈信息
				wx.onMenuShareTimeline({
					title: title,
					link: buyUrl,
					imgUrl: imgUrl,
					trigger: function (res) {
						// 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
						//alert('用户点击分享到朋友圈');
					},
					success: function (res) {
						alert('感谢您的分享');
						if( $.isFunction(callback) ){
							callback();
						}
					},
					cancel: function (res) {
						alert('真可惜!还请下次分享到您的朋友圈哦!');
					},
					fail: function (res) {
						alert("分享失败了!"+JSON.stringify(res));
					}
				});
				//分享到QQ
				wx.onMenuShareQQ({
					title: title,
					desc: desc,
					link: buyUrl,
					imgUrl: imgUrl,
					trigger: function (res) {
						//alert('用户点击分享到QQ');
					},
					complete: function (res) {
						// alert(JSON.stringify(res));
					},
					success: function (res) {
						alert('感谢您的分享');
						if( $.isFunction(callback) ){
							callback();
						}
					},
					cancel: function (res) {
						alert('真可惜!还请下次介绍给您的朋友哦!');
					},
					fail: function (res) {
						alert("分享失败了!"+JSON.stringify(res));
					}
				});
			
				//分享到微博
				wx.onMenuShareWeibo({
					title: title,
					desc: desc,
					link: buyUrl,
					imgUrl: imgUrl,
					trigger: function (res) {
						//alert('用户点击分享到微博');
					},
					complete: function (res) {
						//alert(JSON.stringify(res));
					},
					success: function (res) {
						alert('感谢您的分享');
						if( $.isFunction(callback) ){
							callback();
						}
					},
					cancel: function (res) {
						alert('真可惜!还请下次介绍给您的朋友哦!');
					},
					fail: function (res) {
						alert("分享失败了!"+JSON.stringify(res));
					}
				});
			});
		},
		error:function(){
			}
	});
}

function getQueryString(name)
{
	 var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	 var r = window.location.search.substr(1).match(reg);
	 if(r!=null){
		 return r[2]; 
	 }
	 return '';
}

function configAppShare(){
	var platform = getQueryString("platform");
	platform = platform.toLowerCase();
	if( platform == "ios" ){
		 /*这段代码是固定的，必须要放到js中*/
        function setupWebViewJavascriptBridge(callback) {
            if (window.WebViewJavascriptBridge) {
                return callback(WebViewJavascriptBridge);
            }
            if (window.WVJBCallbacks) {
                return window.WVJBCallbacks.push(callback);
            }
            window.WVJBCallbacks = [callback];
            var WVJBIframe = document.createElement('iframe');
            WVJBIframe.style.display = 'none';
            WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__';
            document.documentElement.appendChild(WVJBIframe);
            setTimeout(function () {
                document.documentElement.removeChild(WVJBIframe)
            }, 0)
        }
		
        //bridge桥注册方法
        var bridgeCallback = function (bridge) {
        	bridge.registerHandler('GetShareParam', function(data, responseCallback) {
        		var responseData = {"MplifeShareWeixinTitle":("share_title" in g_share_info)?g_share_info["share_title"]:"",
									"MplifeShareWeixinDesc":("share_desc" in g_share_info)?g_share_info["share_desc"]:"",
									"MplifeShareWeixinImageUrl":("share_img_url" in g_share_info)?g_share_info["share_img_url"]:"",
									"MplifeShareWeixinUrl":("share_url" in g_share_info)?g_share_info["share_url"]:""
								   };
        		responseCallback(responseData);
        	});
        }

        /*与OC交互的所有JS方法都要放在此处注册，才能调用通过JS调用OC或者让OC调用这里的JS*/
        setupWebViewJavascriptBridge(bridgeCallback);
	}else if( platform == "android" ){

	}
}

function AndroidGetShareParam(data){
	var responseData = {"MplifeShareWeixinTitle":("share_title" in g_share_info)?encodeURI(g_share_info["share_title"]):"",
						"MplifeShareWeixinDesc":("share_desc" in g_share_info)?encodeURI(g_share_info["share_desc"]):"",
						"MplifeShareWeixinImageUrl":("share_img_url" in g_share_info)?encodeURI(g_share_info["share_img_url"]):"",
						"MplifeShareWeixinUrl":("share_url" in g_share_info)?encodeURI(g_share_info["share_url"]):""
					   };
	window.js.GetShareContent(JSON.stringify(responseData));
}